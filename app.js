const {sequelize, User, Post} = require ('./models');
const { v4: uuidv4 } = require('uuid');

const express = require ('express');
// const User = require('./models');

const app = express();
app.use(express.json());


app.post ('/users', async (req, res) => {
    const {name, email, number} = req.body;
    const uuid = uuidv4();
    try {
        const users = await User.create ({ uuid, name , email, number});
        return res.json(users);
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
})

app.get('/users', async (req, res) => {
    try {
        const users = await User.findAll();
        // console.log(users);
        return res.json(users);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: "Something went wrong"})
    }
})

app.get ('/users/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const users = await User.findOne({
            where: { id }
        });
        if (users !== null) {
            return res.json(users);
        }else {
            return res.json({});
        } 
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            error: "Something went wrong"
        })
    }
})


app.listen({port : 5000}, async () => {
    console.log("Server up on http://localhost:5000 ");
    await sequelize.authenticate();
    console.log('Database Connected');
})